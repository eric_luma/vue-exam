import VueRouter from 'vue-router';
import Vue from 'vue';

import Login from './components/Login';
import NotFound from './components/NotFound';
import UsersView from './components/UsersView';
import UsersEdit from './components/UsersEdit';

const routes = [
    {
        path: '/login',
        component: Login
    },
    {
        path: '/users',
        component: UsersView
    },
    {
        path: '/users/:username',
        component: UsersEdit
    },
    {
        path: '*',
        component: NotFound
    }
];

Vue.use(VueRouter);

const router = new VueRouter({
    routes: routes
});

router.beforeEach((to, from, next) => {

    if(!sessionStorage.getItem('username') && to.path != '/login') {

        next('/login');
    } else {

        next();
    }
});

export default router;
