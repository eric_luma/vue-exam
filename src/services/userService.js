import * as users from '../../db/db.json';

export default {
  getUsers() {

    return new Promise((resolve) => {

      resolve(users);
    });
  },
  getUserByUserName(username) {

    return new Promise((resolve, reject) => {

      let userFound = users.filter(user => {
        return user.usuario == username;
      });

      if (userFound.length) {

        resolve(userFound[0]);
      } else {

        reject();
      }
    });
  }
}
