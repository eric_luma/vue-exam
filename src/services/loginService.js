import * as users from '../../db/db.json';

export default {
  login(usuario, pass) {

    return new Promise((resolve, reject) => {

      let foundUser = users.filter(user => {
        return user.usuario == usuario && user.clave == pass;
      });

      if (foundUser.length) {
        resolve(foundUser[0]);
      } else {
        reject();
      }
    });
  }
}
