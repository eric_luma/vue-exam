import Vue from 'vue'
import App from './App.vue'
import router from './app.router';

Vue.config.productionTip = false

Vue.filter('nombreApellido', (values) => {
  return `${values[0]} ${values[1]}`;
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
